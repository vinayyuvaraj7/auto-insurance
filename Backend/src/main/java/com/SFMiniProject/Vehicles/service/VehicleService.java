package com.SFMiniProject.Vehicles.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.SFMiniProject.Vehicles.exception.vehicleNotFoundException;
import com.SFMiniProject.Vehicles.model.Vehicle;
import com.SFMiniProject.Vehicles.repo.VehicleRepo;

@Service
@Transactional
public class VehicleService {

	private final VehicleRepo vehicleRepo;

    @Autowired
    public VehicleService(VehicleRepo vehicleRepo){
        this.vehicleRepo = vehicleRepo;
    }

    public Vehicle addVehicle(Vehicle vehicle){
        return vehicleRepo.save(vehicle);
    }

    public List<Vehicle> findAllVehicles(){
        return vehicleRepo.findAll();
    }

    public Vehicle updateVehicle(Vehicle vehicle){
        return vehicleRepo.save(vehicle);
    }

    public void deleteVehicle(Long id){
        vehicleRepo.deleteVehicleByid(id);
    }

    public Vehicle findVehicleById(Long id){
        return vehicleRepo.findVehicleById(id)
                .orElseThrow(()-> new vehicleNotFoundException("Vehicle by id "+ id + " was not found."));
    }
}
