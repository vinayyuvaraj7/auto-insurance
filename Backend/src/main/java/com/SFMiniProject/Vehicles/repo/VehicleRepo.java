package com.SFMiniProject.Vehicles.repo;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SFMiniProject.Vehicles.model.Vehicle;

public interface VehicleRepo extends JpaRepository<Vehicle, Long>{

	void deleteVehicleByid(Long id);

    Optional<Vehicle> findVehicleById(Long id);
}
