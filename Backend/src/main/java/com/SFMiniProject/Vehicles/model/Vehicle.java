package com.SFMiniProject.Vehicles.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity
public class Vehicle implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private long id;
	@Column(nullable = false)
    private String year;
	@Column(nullable = false)
    private String make;
	@Column(nullable = false)
    private String model;
	@Column(nullable = false)
    private String bodyStyle;
	
	
	public Vehicle() {}

	public Vehicle(long id, String year, String make, String model, String bodyStyle) {
		super();
		this.id = id;
		this.year = year;
		this.make = make;
		this.model = model;
		this.bodyStyle = bodyStyle;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBodyStyle() {
		return bodyStyle;
	}
	public void setBodyStyle(String bodyStyle) {
		this.bodyStyle = bodyStyle;
	}

	
}
