import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DriverComponent } from './driver-info/driver/driver.component';

import { HomePageComponent } from './home-page/home-page.component';
import { HeaderBarComponent } from './personal-info/header-bar/header-bar.component';
import { VehiclesListComponent } from './vehicle-info/vehicles-list/vehicles-list.component';

const routes: Routes = [
   { path: 'home-page', component: HomePageComponent  },
   { path: 'personal-info', component:  HeaderBarComponent},
   { path: 'vehicle-info', component:  VehiclesListComponent},
   { path: 'driver-info', component:  DriverComponent},
   { path: '', redirectTo: 'home-page', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
