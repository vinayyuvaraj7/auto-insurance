import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personal-info-form',
  templateUrl: './personal-info-form.component.html',
  styleUrls: ['./personal-info-form.component.css']
})
export class PersonalInfoFormComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  save(){
    this.router.navigate(['/vehicle-info']);
  }

}
