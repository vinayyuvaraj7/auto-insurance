import { Component, OnInit } from '@angular/core';

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-product-selection',
  templateUrl: './product-selection.component.html',
  styleUrls: ['./product-selection.component.css']
})
export class ProductSelectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  selectedValue!: string;
  selectedCar!: string;

  foods: Food[] = [
    {value: 'auto-0', viewValue: 'Auto'},
    {value: 'life-1', viewValue: 'Life'},
    {value: 'home-2', viewValue: 'Home'},
  ];

  router: any;
  navigateTo(value: any){
    console.log(value);
    this.router.navigate(['../',value]);
  }

}
