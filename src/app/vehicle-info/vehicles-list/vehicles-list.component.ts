import { Component, OnInit, ViewChild } from '@angular/core';
import { AddVehiclesComponent } from '../add-vehicles/add-vehicles.component';
import { VehicleService } from 'src/app/services/vehicle.service';
import { NotificationService } from 'src/app/services/notification.service';
import { DialogService } from 'src/app/services/dialog.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Vehicle } from '../vehicle';
import { HttpErrorResponse } from '@angular/common/http';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-vehicles-list',
  templateUrl: './vehicles-list.component.html',
  styleUrls: ['./vehicles-list.component.css']
})
export class VehiclesListComponent implements OnInit {

  public vehicles!: Vehicle[];
  public editVehicle!: Vehicle;
  public deleteVehicle!: Vehicle;
  public static test : any;

  constructor(public service: VehicleService, public dialog: MatDialog, 
    public notificationService: NotificationService, public dialogService: DialogService,
    public router: Router, private eventEmitterService: EventEmitterService) { }
  isDisable:boolean = true;
  array!: [];
  listData!: MatTableDataSource<any>;
  displayedColumns: any[] = ['year', 'make', 'model', 'bodyStyle', 'actions'];

  ngOnInit(): void {
    this.getVehicles();
    this.eventEmitterService.notifyObservable$.subscribe(res => {
      if(res.refresh){
          // get your grid data again. Grid will refresh automatically
          this.delay(1000).then(any=>{
            this.getVehicles();
       });
      }
  })
}

async delay(ms: number) {
  await new Promise<void>(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("fired"));
}

  public getVehicles(): any{
     this.service.getVehicles().subscribe(
      (response: Vehicle[]) => {this.vehicles = response;
        VehiclesListComponent.test = response.length;
        if(VehiclesListComponent.test>=1){
          this.isDisable = false;
        }  
        else{
          this.isDisable = true;
        }
      },
      (error: HttpErrorResponse) => {alert(error.message);}
    );

    return this.vehicles;
  }

    onCreate(){
      console.log(this.vehicles.length);
    if(VehiclesListComponent.test<4){
      this.service.initializeFormGroup();
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = "35%";
      dialogConfig.height = "90%";
      this.dialog.open(AddVehiclesComponent, dialogConfig);
    }
    else{
       this.notificationService.warn('You can only add upto 4 Vehicles!');
    }

  }

    onEdit(row: any){
      this.service.populateForm(row);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = "35%";
      dialogConfig.height = "90%";
      this.dialog.open(AddVehiclesComponent, dialogConfig);
    }

    onDelete(vehicleId: number){
      this.dialogService.openConfirmDialog("Are you sure to delete this record?")
      .afterClosed().subscribe(res => {
      if(res){
        this.service.deleteVehicle(vehicleId).subscribe(
          (response: void) => {
            console.log(response);
            this.getVehicles();
          },
          (error: HttpErrorResponse) => {
            alert(error.message);
          }
        );
        this.notificationService.warn('! Vehicle details removed Successfully');
      }
    });
  }

  save(){
    this.router.navigate(['/driver-info']);
  }

  prev(){
    this.router.navigate(['/personal-info']);
  }

}
