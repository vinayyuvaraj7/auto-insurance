export interface Vehicle{
    id: number;
    year: string;
    make: string;
    model: string;
    bodyStyle: string;
}