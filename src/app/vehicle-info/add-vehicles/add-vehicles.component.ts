import { Component, OnInit } from '@angular/core';
import { VehicleService } from 'src/app/services/vehicle.service';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from 'src/app/services/notification.service';
import { Vehicle } from '../vehicle';
import { HttpErrorResponse } from '@angular/common/http';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-add-vehicles',
  templateUrl: './add-vehicles.component.html',
  styleUrls: ['./add-vehicles.component.css']
})
export class AddVehiclesComponent implements OnInit {

  vehicles!: Vehicle[];
  constructor(public service: VehicleService, public dialogRef: MatDialogRef<AddVehiclesComponent>,
    public notificationService: NotificationService, private eventEmitterService: EventEmitterService) { }

  disableForm : boolean = true;

  ngOnInit(): void {
    this.service.form.get('make')?.disable();
    this.service.form.get('model')?.disable();
    this.service.form.get('bodyStyle')?.disable();
    this.service.getVehicles();
    this.makeList = this.make();
  }


  yearList = ['2022', '2021', '2020', '2019', '2018', '2017', '2016', '2015', '2014',
  '2013', '2012', '2011', '2010', '2009', '2008', '2007', '2006', '2005', '2004',
 '2003', '2002', '2001', '2000'];

  makeList:any = [];
  modelList:any = [];

  make(){ 
    return ['AUDI', 'BMW' , 'CADILLAC' , 
    'CHEVROLET' , 'HONDA' , 'HYUNDAI' ,
    'JAGUAR' , 'JEEP' , 'KIA' , 
    'LAMBORGHINI' , 'LAND ROVER' , 'MASERATI' , 
    'MITSUBISHI' , 'NISSAN' ,'TESLA' , 
    'TOYOTA' , 'VOLKSWAGEN' , 'VOLVO'];
}

  model(){ 
    return [{id:'AUDI',value:'Q3'}, {id:'AUDI',value:'Q5'}, {id:'AUDI',value:'Q7'}, {id:'AUDI',value:'A5'}, {id:'AUDI',value:'A7'},
      {id:'BMW',value:'ALPINA B7'}, {id:'BMW',value:'X2'}, {id:'BMW',value:'X3'}, {id:'BMW',value:'X4'}, {id:'BMW',value:'X5'},
    {id:'CADILLAC',value:'CT4'}, {id:'CADILLAC',value:'CT5'}, {id:'CADILLAC',value:'XT4'}, {id:'CADILLAC',value:'XT5'}, {id:'CADILLAC',value:'XT6'},
    {id:'CHEVROLET',value:'EQUINOX'}, {id:'CHEVROLET',value:'BOLT'}, {id:'CHEVROLET',value:'CAMARO'}, {id:'CHEVROLET',value:'MALIBU'}, {id:'CHEVROLET',value:'SPARK'},
    {id:'HONDA',value:'CIVIC'}, {id:'HONDA',value:'HR V'}, {id:'HONDA',value:'INSIGHT'}, {id:'HONDA',value:'ODYSSEY'}, {id:'HONDA',value:'PILOT'},
    {id:'HYUNDAI',value:'ELANTRA'}, {id:'HYUNDAI',value:'KONA'}, {id:'HYUNDAI',value:'PALISADE'}, {id:'HYUNDAI',value:'SANTA CRUZ'}, {id:'HYUNDAI',value:'SONATA'},
    {id:'JAGUAR',value:'F-PACE'}, {id:'JAGUAR',value:'F-TYPE'}, {id:'JAGUAR',value:'I-PACE'}, {id:'JAGUAR',value:'XF'},
    {id:'JEEP',value:'COMPASS'}, {id:'JEEP',value:'WAGONEER'},
    {id:'KIA',value:'CARNIVAL'}, {id:'KIA',value:'K5'}, {id:'KIA',value:'SENTOS'}, {id:'KIA',value:'SOUL'}, {id:'KIA',value:'SPORTAGE'},
    {id:'LAMBORGHINI',value:'URUS'},
    {id:'LAND ROVER',value:'DEFENDER'}, {id:'LAND ROVER',value:'DISCOVERY'}, {id:'LAND ROVER',value:'RANGE ROVER SPORT'},
    {id:'MASERATI',value:'LEVANTE'},
    {id:'MITSUBISHI',value:'ECLIPSE CROSS'}, {id:'MITSUBISHI',value:'MIRAGE'}, {id:'MITSUBISHI',value:'OUTLANDER'},
    {id:'NISSAN',value:'LEAF ELECTRIC'}, {id:'NISSAN',value:'PATH FINDER'},
    {id:'TESLA',value:'MODEL S'}, {id:'TESLA',value:'MODEL X'}, {id:'TESLA',value:'MODEL 3'}, {id:'TESLA',value:'Y'},
    {id:'TOYOTA',value:'CAMRY'}, {id:'TOYOTA',value:'COROLLA'}, {id:'TOYOTA',value:'GR 86'}, {id:'TOYOTA',value:'HIGHLANDER'}, {id:'TOYOTA',value:'SUPRA'},
    {id:'VOLKSWAGEN',value:'PASSAT'}, {id:'VOLKSWAGEN',value:'TAOS'},
    {id:'VOLVO',value:'S60'}, {id:'VOLVO',value:'V60'}];
}

stylesList = ['4D SED GAS', 'AWD 4D GAS', 'SPORT VAN GAS', 'CPE GAS', 'CV GAS', '4DR GAS'];

  selectMake(makeList:any){
    if(!this.service.form.get('make')?.hasError('required')){
      this.service.form.get('model')?.enable();
    }
    this.modelList = this.model().filter(e => e.id == this.service.form.value['make']);
  }

  selectYear(){
    if(!this.service.form.get('year')?.hasError('required')){
      this.service.form.get('make')?.enable();
    }
  }

    selectModel(){
    if(!this.service.form.get('model')?.hasError('required')){
      this.service.form.get('bodyStyle')?.enable();
    }
  }

    selectStyle(){
      if(!this.service.form.get('bodyStyle')?.hasError('required')){
        this.disableForm = false;
      }
    }

    onSubmit(){
      if(this.service.form.valid){
        if(!this.service.form.get('id')?.value){
          this.service.insertVehicle(this.service.form.value).subscribe(
            (response: Vehicle) => {
              console.log(response);
              this.service.getVehicles();
              this.service.form.reset();
            },
            (error: HttpErrorResponse) => {
              alert(error.message);
              this.service.form.reset();
            }
          );
          this.notificationService.success('Vehicle Details added Successfully!');
        }
        else{
          this.service.updateVehicle(this.service.form.value).subscribe(
            (response: Vehicle) => {
              console.log(response);
              this.service.getVehicles();
            },
            (error: HttpErrorResponse) => {
              alert(error.message);
            }
          );
          this.notificationService.success('Vehicle Details updated Successfully!');
        }
        this.eventEmitterService.notifyOther({refresh: true})
        this.service.initializeFormGroup();
        this.onClose();
      }
    }

    onClose(){
      this.service.form.reset();
      this.service.initializeFormGroup();
      this.dialogRef.close();
    }

}
