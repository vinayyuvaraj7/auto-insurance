import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/*Home Page and Personal Info*/
import { HomePageComponent } from './home-page/home-page.component';
import { ProductSelectionComponent } from './home-page/product-selection/product-selection.component';
import { PersonalInfoFormComponent } from './personal-info/personal-info-form/personal-info-form.component';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


/*Vehicle Info*/
import { AddVehiclesComponent } from './vehicle-info/add-vehicles/add-vehicles.component';
import { VehiclesListComponent } from './vehicle-info/vehicles-list/vehicles-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

/*Driver Info*/
import { DriverComponent } from './driver-info/driver/driver.component';

/*Angular Material Modules*/
import { MaterialModule } from './material/material.module';
import { HeaderBarComponent } from './personal-info/header-bar/header-bar.component';
import { MatConfirmDialogComponent } from './vehicle-info/mat-confirm-dialog/mat-confirm-dialog.component';
import { VehicleService } from './services/vehicle.service';
import { EventEmitterService } from './services/event-emitter.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ProductSelectionComponent,
    PersonalInfoFormComponent,
    AddVehiclesComponent,
    VehiclesListComponent,
    DriverComponent,
    HeaderBarComponent,
    MatConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    NoopAnimationsModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    VehicleService,
    EventEmitterService
  ],
  bootstrap: [AppComponent],
  entryComponents: [VehiclesListComponent]
})
export class AppModule { }
