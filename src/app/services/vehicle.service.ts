import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Vehicle } from '../vehicle-info/vehicle';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  form: FormGroup = new FormGroup({
    id: new FormControl(''),
    year: new FormControl('', Validators.required),
    make: new FormControl({value: '', disabled: true}, Validators.required),
    model: new FormControl({value: '', disabled: true}, Validators.required),
    bodyStyle: new FormControl({value: '', disabled: true}, Validators.required)
  });

  initializeFormGroup(){
    this.form.setValue({
      id:'',
      year: '',
      make: '',
      model: '',
      bodyStyle: ''
    });
  }

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(`${this.apiServerUrl}/vehicle/all`);
  }

  public insertVehicle(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.post<Vehicle>(`${this.apiServerUrl}/vehicle/add`, vehicle);
  }

  public updateVehicle(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.put<Vehicle>(`${this.apiServerUrl}/vehicle/update`, vehicle);
  }

  public deleteVehicle(vehicleId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/vehicle/delete/${vehicleId}`);
  }

  populateForm(vehicle: any){
    this.form.setValue(vehicle);
  }

}
