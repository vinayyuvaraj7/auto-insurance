import { Injectable, EventEmitter } from '@angular/core';    
import { Subscription } from 'rxjs/internal/Subscription';
import {BehaviorSubject} from 'rxjs'; 
    
@Injectable({    
  providedIn: 'root'    
})    
export class EventEmitterService {    
    
  public notify = new BehaviorSubject<any>('');

  notifyObservable$ = this.notify.asObservable();

    public notifyOther(data: any) {
    if (data) {
        this.notify.next(data);
    }
}  
} 