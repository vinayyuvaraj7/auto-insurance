import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  constructor() { }

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    fname: new FormControl('', Validators.required),
    /*email: new FormControl('', Validators.email),*/
    lname: new FormControl('', Validators.required),
    city:new FormControl('',Validators.required),
    state:new FormControl('',Validators.required),
    pin:new FormControl('',[Validators.required,Validators.minLength(5),Validators.maxLength(5)]),
    email: new FormControl('', Validators.email),
    license: new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(10)]),
    mobile: new FormControl('', [Validators.required, Validators.minLength(10)]),
    /*city: new FormControl(''),*/
    gender: new FormControl('1'),
    dept: new FormControl(0),
    hireDate: new FormControl(''),
    isPermanent: new FormControl(false)
  });

}
